#!/usr/bin/env python3

from re import split
import os.path

f_num = input("Choose a file:")
if not os.path.isfile("locations_test/locs" + f_num):
    print("File not found")
    exit()

loc_tested = set([])
if os.path.isfile("locations_test/locs" + f_num + "_tested"):
    for l in open("locations_test/locs" + f_num + "_tested"):
        loc_tested.add(split("\t", l.strip())[0])

locs_not_tested = {}
for l in open("locations_test/locs" + f_num):
    s = split("\t", l.strip())
    if s[0] in loc_tested:
        continue
    else:
        locs_not_tested[s[0]] = s[1]

print('''
Inputs:
    Enter for true detection
    'n' and Enter for false detection
    'no' and Enter if detection quality is not obviously
    'sl' and Enter if there are several locations, but detected one of theme
    'q' for exit
''')

f_out = open("locations_test/locs" + f_num + "_tested", "a")
for loc in sorted(locs_not_tested.keys()):
    valid_res = False
    while not valid_res:
        f_out.flush()
        valid_res = True
        res = input(loc + " --- " + locs_not_tested[loc] + ": ")
        if res == "":
            f_out.write(loc + "\t" + locs_not_tested[loc] + "\tt\n")
        elif res == "n":
            f_out.write(loc + "\t" + locs_not_tested[loc] + "\tf\n")
        elif res == "no":
            f_out.write(loc + "\t" + locs_not_tested[loc] + "\tno\n")
        elif res == "sl":
            f_out.write(loc + "\t" + locs_not_tested[loc] + "\tsl\n")
        elif res == "q":
            f_out.close()
            exit()
        else:
            print("Invalid answer. Once again:")
            valid_res = False

print("File is tested!")
